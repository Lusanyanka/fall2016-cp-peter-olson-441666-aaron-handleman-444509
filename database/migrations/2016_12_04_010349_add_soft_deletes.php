<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSoftDeletes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('campaign_user', function($table){
            $table->softDeletes();
        });
        Schema::table('characters', function($table){
            $table->softDeletes();
        });
        Schema::table('campaigns', function($table){
            $table->softDeletes();
        });
        Schema::table('items', function($table){
            $table->softDeletes();
        });
        Schema::table('character_spell', function($table){
            $table->softDeletes();
        });
        Schema::table('character_item', function($table){
            $table->softDeletes();
        });
        Schema::table('spells', function($table){
            $table->softDeletes();
        });
        Schema::table('rolls', function($table){
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('campaign_user', function($table){
            $table->dropColumn('deleted_at');
        });
        Schema::table('characters', function($table){
            $table->dropColumn('deleted_at');
        });
        Schema::table('campaigns', function($table){
            $table->dropColumn('deleted_at');
        });
        Schema::table('items', function($table){
            $table->dropColumn('deleted_at');
        });
        Schema::table('character_spell', function($table){
            $table->dropColumn('deleted_at');
        });
        Schema::table('character_item', function($table){
            $table->dropColumn('deleted_at');
        });
        Schema::table('spells', function($table){
            $table->dropColumn('deleted_at');
        });
        Schema::table('rolls', function($table){
            $table->dropColumn('deleted_at');
        });
    }
}
