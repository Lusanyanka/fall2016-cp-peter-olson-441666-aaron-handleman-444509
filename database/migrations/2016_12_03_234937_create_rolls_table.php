<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRollsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rolls', function(Blueprint $table){
            $table->increments('id');
            $table->timestamps();
            $table->integer('user_id')->unsigned();
            $table->integer('campaign_id')->unsigned();
            $table->unsignedTinyInteger('roll_1')->default(8);
            $table->unsignedTinyInteger('roll_2')->default(8);
            $table->unsignedTinyInteger('roll_3')->default(8);
            $table->unsignedTinyInteger('roll_4')->default(8);
            $table->unsignedTinyInteger('roll_5')->default(8);
            $table->unsignedTinyInteger('roll_6')->default(8);
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('campaign_id')->references('id')->on('campaigns');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rolls');
    }
}
