<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCharacterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('characters', function(Blueprint $table){
            $table->increments('id');
            $table->timestamps();
            $table->integer('user_id')->unsigned();
            $table->integer('campaign_id')->unsigned();
            $table->string('name', 255);
            $table->smallInteger('max_health')->unsigned()->default(0);
            $table->unsignedTinyInteger('health_d6')->default(0);
            $table->unsignedTinyInteger('health_d8')->default(0);
            $table->unsignedTinyInteger('health_d10')->default(0);
            $table->unsignedTinyInteger('health_d12')->default(0);
            $table->unsignedTinyInteger('strength')->default(8);
            $table->unsignedTinyInteger('dexterity')->default(8);
            $table->unsignedTinyInteger('constitution')->default(8);
            $table->unsignedTinyInteger('intelligence')->default(8);
            $table->unsignedTinyInteger('wisdom')->default(8);
            $table->unsignedTinyInteger('charisma')->default(8);
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('campaign_id')->references('id')->on('campaigns');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('characters');
    }
}
