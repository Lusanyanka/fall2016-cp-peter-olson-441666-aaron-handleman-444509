<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCharacterSpellTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('character_spell', function(Blueprint $table){
            $table->increments('id');
            $table->timestamps();
            $table->integer('character_id')->unsigned();
            $table->integer('spell_id')->unsigned();
            $table->unique(['character_id', 'spell_id']);
            $table->foreign('character_id')->references('id')->on('characters');
            $table->foreign('spell_id')->references('id')->on('spells');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('character_spell');
    }
}
