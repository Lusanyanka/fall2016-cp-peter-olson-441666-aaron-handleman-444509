# DnD Sheet Manager #
Aaron Handleman and Peter Olson's CSE330 creative project

## Adding Homebrew Content - (15pts) ##
 * (5pts) GM can create new gear/spells
 * (5pts) Gear/spells has/have associated stats that are displayed along with the gear
 * (5pts) Existing equipment/spells can be used as a template/templates for new gear/spells

## Character Creation - (25pts) ##
 * (5pts) Random number generator is minimally statistically biased
 * (2pts) The GM can see a log of players attempting character creation
 * (3pts) Option to enforce hardcore rolling
 * (10pts) Automate the process of getting initial money, gear, choosing skills, etc
 * (5pts) Players can select skills using pointbuy with rules defined by the GM

## Community and Game Management - (30pts) ##
 * (5pts) Pool of currently active games and short descriptions
 * (2pts) Each game has a roster of players
 * (4pts) The GM can set which fields players can and cannot see on eachother's sheets
 * (4pts) The GM can set which fields should and should not be displayed for a particular campaign
 * (10pts) Player profiles with games they're playing and GMing in
 * (5pts) Players in a game can see sheets of other players in that game
 
## Creative Portion - (20pts) ##

All of the work that we did in the Vue templates is our creative portion. It's a whole new Javascript framework, and we did some pretty slick things with it.