<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Appropriate, because we don't want a landing page to respond to anything
// but for a GET request
Route::get('/', 'LandingController@index')->name('BabyGotBackSlash');

Route::get('/test_aaron', function() {
	return view('test_aaron');
});

Route::get('/test_layout', function() {
	return view('home_aaron');
})->name('home_aaron');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('character', 'CharacterController');
Route::resource('roll', 'RollController');
Route::resource('campaign', 'CampaignController');
Route::resource('spell', 'SpellController');
Route::resource('item', 'ItemController');
Route::get('testing_form_aaron', function() {
	if (Auth::guest()) {
		return redirect()->route('login');
	} else {
		return view('testing_form_aaron');
	}
})->name('testing_form_aaron');

Route::post('campaign/uniqueAdd', 'CampaignController@uniqueAdd')->name('uniqueAdd');
Route::post('character/create', 'CharacterController@specialCreate');
Route::get('campaign/add/{id}', 'CampaignController@addUser');
/*
 * Peter's junk
 */

Route::get('/test_peter', function() {
	// Simple table for testing
	$pets = ['cats','dogs','fish'];
	return view('test_peter', compact('pets'));
});

Route::get('/test_peter/{indentifier}', 'PeterTestController@id_response');

Route::get('/test_peter/cTest', 'PeterTestController@index');

Route::get('rubric', function(){
	return view('welcome');
});

Route::get('/item/create/{id}', 'ItemController@template');
Route::get('/spell/create/{id}', 'SpellController@template');
Route::get('/user', 'UserController@index')->name('user');
Route::get('/user/{id}', 'UserController@show');