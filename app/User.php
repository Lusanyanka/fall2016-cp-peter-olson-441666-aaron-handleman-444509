<?php
//Ascii art from  http://lunicode.com/bigtext

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


/*
______             _                ___                                       
|  ___|           (_)              / _ \                                      
| |_ ___  _ __ ___ _  __ _ _ __   / /_\ \ ___ ___ ___  ___ ___  ___  _ __ ___ 
|  _/ _ \| '__/ _ \ |/ _` | '_ \  |  _  |/ __/ __/ _ \/ __/ __|/ _ \| '__/ __|
| || (_) | | |  __/ | (_| | | | | | | | | (_| (_|  __/\__ \__ \ (_) | |  \__ \
\_| \___/|_|  \___|_|\__, |_| |_| \_| |_/\___\___\___||___/___/\___/|_|  |___/
                      __/ |                                                   
                     |___/                                                    
*/
    public function campaigns() {
    	return $this->hasMany('App\Campaign');
    }
    public function characters() {
    	return $this->hasMany('App\Character');
    }
    public function rolls() {
    	return $this->hasMany('App\Roll');
    }
    public function items()
    {
        return $this->hasMany('App\Item');
    }
    public function spells()
    {
        return $this->hasMany('App\Spell');
    }
}
