<?php
//Ascii art from  http://lunicode.com/bigtext

namespace App;

use Illuminate\Database\Eloquent\Model;

class Roll extends Model
{



	
/*
______             _                ___                                       
|  ___|           (_)              / _ \                                      
| |_ ___  _ __ ___ _  __ _ _ __   / /_\ \ ___ ___ ___  ___ ___  ___  _ __ ___ 
|  _/ _ \| '__/ _ \ |/ _` | '_ \  |  _  |/ __/ __/ _ \/ __/ __|/ _ \| '__/ __|
| || (_) | | |  __/ | (_| | | | | | | | | (_| (_|  __/\__ \__ \ (_) | |  \__ \
\_| \___/|_|  \___|_|\__, |_| |_| \_| |_/\___\___\___||___/___/\___/|_|  |___/
                      __/ |                                                   
                     |___/                                                    
*/
    public function user() {
    	return $this->hasOne('App\User');
    }
}
