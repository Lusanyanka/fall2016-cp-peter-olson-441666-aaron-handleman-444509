<?php

namespace App\Http\Controllers;

class PeterTestController extends Controller
{
    public function index(){
		$pets = ['cats','dogs','fish'];
		return view('test_peter', compact('pets'));
    }

    public function id_response($identifier){
    	return view('test_peter');
    }
}
