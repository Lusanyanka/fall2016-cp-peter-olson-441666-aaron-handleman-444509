<?php

namespace App\Http\Controllers;

use App\User;
use App\Campaign;
use App\Roll;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RollController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        function rolling($rolled = 4, $drop = 1) {
            if(!isset($rolled)) {
                $rolled = 4;
            }
            if(!isset($drop)) {
                $drop = 1;
            }
            $error_message = sprintf("Rolled = %d and Drop = %d", $rolled, $drop);
            $rand_rolls = array();
            for($i = 0; $i < $rolled; $i++){
                $rand_rolls[$i] =  mt_rand(1, 6);
            }
            $rand_rolls = collect($rand_rolls);
            for ($i=0; $i < $drop; $i++) { 
             $rand_rolls->forget($rand_rolls->search($rand_rolls->min()));
         }

         $roll_total = $rand_rolls->sum();
         return $roll_total;
     }
     if ($request->has('campaign')) {
        $roll = new Roll;
        if (($request->has('r1')) && ($request->has('r2')) && ($request->has('r3')) && ($request->has('r4')) && ($request->has('r5')) && ($request->has('r6'))) {
            $roll->roll_1 = $request->r1;
            $roll->roll_2 = $request->r2;
            $roll->roll_3 = $request->r3;
            $roll->roll_4 = $request->r4;
            $roll->roll_5 = $request->r5;
            $roll->roll_6 = $request->r6;
        } else {
            $rolled = $request->rolled;
            $drop = $request->rolled;
            $roll->roll_1 = rolling($rolled, $drop);
            $roll->roll_2 = rolling($rolled, $drop);
            $roll->roll_3 = rolling($rolled, $drop);
            $roll->roll_4 = rolling($rolled, $drop);
            $roll->roll_5 = rolling($rolled, $drop);
            $roll->roll_6 = rolling($rolled, $drop);
        }
        $roll->user_id = Auth::user()->id;
        $roll->campaign_id = intval($request->campaign);
        $roll->save();

        return redirect()->route('roll.show', ['roll' => $roll->id]);
    } else {
        error_log($request);
        return redirect()->route('testing_form_aaron');
    }
}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('shows.roll_show_aaron', ['roll' => Roll::find($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
