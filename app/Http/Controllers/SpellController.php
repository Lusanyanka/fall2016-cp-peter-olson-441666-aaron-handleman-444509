<?php

namespace App\Http\Controllers;

use App\User;
use App\Spell;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SpellController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::check()){
            $spells = Auth::user()->spells()
            ->orderBy('updated_at')
            ->simplePaginate(16);
            return view('spell.index', compact('spells'));
        } else{
            return redirect()->route('BabyGotBackSlash');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::check()){
            return view('spell.create');
        } else{
            return redirect()->route('BabyGotBackSlash');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->has('name') && $request->has('creator')) {
            $spell = new Spell;
            $spell->name = $request->name;
            $spell->user_id = Auth::user()->id;
            $spell->description = $request->description;
            $spell->save();
            return redirect()->route('spell.index');
        } else {
            return redirect()->route('BabyGotBackSlash');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function template($id)
    {
        if(Auth::check() && Spell::find($id)){
            return view('spell.create', compact('id'));
        } else{
            return redirect()->route('spell.index');
        }
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
