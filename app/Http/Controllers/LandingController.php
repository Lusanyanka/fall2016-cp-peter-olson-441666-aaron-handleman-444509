<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LandingController extends Controller
{
    public function index(){
    	$games = \App\Campaign::orderBy('updated_at')->take(15)->get();
    	return view('landing', compact('games'));
    }
}
