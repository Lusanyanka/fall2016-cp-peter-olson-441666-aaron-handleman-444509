<?php

namespace App\Http\Controllers;

use App\User;
use App\Item;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::check()){
            $items = Auth::user()->items()
            ->orderBy('updated_at')
            ->simplePaginate(16);
            return view('item.index', compact('items'));
        } else{
            return redirect()->route('BabyGotBackSlash');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::check()){
            return view('item.create');
        } else{
            return redirect()->route('BabyGotBackSlash');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Auth::check() && $request->has('name') && $request->has('description')){
            $item = new Item;
            $item->user_id = Auth::user()->id;
            $item->default_material = false;
            if(isset($request->cost)){
                $item->cost = (int) $request->cost;
            } else {$item->cost = 0;}
            $item->name = $request->name;
            $item->description = $request->description;
            $item->save();
            return redirect()->route('item.index');
        } else{
            return redirect()->route('BabyGotBackSlash');
        }
    }

    public function template($id)
    {
        if(Auth::check() && Item::find($id)){
            return view('item.create', compact('id'));
        } else{
            return redirect()->route('item.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
