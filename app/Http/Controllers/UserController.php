<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\User;

class UserController extends Controller
{
    public function index(){
    	if(Auth::check()){
    		return view('user.index');
    	}
    	else{
    		return redirect()->route('BabyGotBackSlash');
    	}
    }

    public function show($id){
    	if (Auth::check() && User::find($id)) {
    		$user = User::find($id);
    		return view('user.show', compact('user'));
    	} else{
    		return redirect()->route('user');
    	}
    }
}
