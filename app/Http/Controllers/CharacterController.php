<?php

namespace App\Http\Controllers;


use App\Campaign;
use App\User;
use App\Roll;
use App\Character;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CharacterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         if(Auth::check()){
            $chars = Auth::user()->characters;
            if (isset($chars)) {
                return view('character.index', compact('chars'));
            }
        }
        return redirect()->route('BabyGotBackSlash');
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function specialCreate(Request $request)
    {
        if($request->has('campaign')){
            $campaign = $request->campaign;
            return view('character.create', compact('campaign'));
        } else{
            return view('campaign.index');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $thatgoodgood = array('Strength', 'Dexterity', 'Constitution', 'Intelligence', 'Wisom', 'Charisma', 'name', 'campaign', 'creator');
        foreach($thatgoodgood as $check) {
            if (!isset($check)) {
                return redirect()->route('testing_form_aaron');
            }
        }
        $character = new Character;
        $character->strength = $request->Strength + $request->str_bonus;
        $character->dexterity = $request->Dexterity + $request->dex_bonus;
        $character->constitution = $request->Constitution + $request->con_bonus;
        $character->intelligence = $request->Intelligence + $request->int_bonus;
        $character->wisdom = $request->Wisdom + $request->wis_bonus;
        $character->charisma = $request->Charisma + $request->cha_bonus;
        $character->name = $request->name;
        $character->user_id = $request->creator;
        $character->campaign_id = $request->campaign;
        $character->description = "A Character description";
        $character->class = "Ubermensch";
        $character->save();
        return redirect()->route('campaign.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $char = Character::find($id);
        if(isset($char)){
            return view('character.show', compact('char'));
        } else{
            return redirect()->route('BabyGotBackSlash');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
