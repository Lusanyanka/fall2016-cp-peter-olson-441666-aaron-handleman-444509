<?php

namespace App\Http\Controllers;

use App\Campaign;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CampaignController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::check()){
            $games = User::find(Auth::user()->id)->campaigns()->get();
            error_log($games);
            return view('campaign.index', compact('games'));
        } else{
            return redirect()->route('BabyGotBackSlash');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::check()){
            return view('campaign.create');
        } else{
            return redirect()->route('BabyGotBackSlash');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->has('name') && $request->has('creator')) {
            $campaign = new Campaign;
            $campaign->name = $request->name;
            $campaign->user_id = User::where('id', $request->creator)->first()->id;
            $campaign->description = $request->description;
            $campaign->save();
            $campaign->users()->save(Auth::user());
            return redirect()->route('home');
        } else {
            return redirect()->route('testing_form_aaron');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $game = Campaign::find($id);
        if(isset($game)){
            return view('campaign.show', compact('game'));
        } else{
            return redirect()->route('BabyGotBackSlash');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Show the form for adding a user.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function addUser($id)
    {
        if(Auth::check() && Campaign::find($id)->user_id === Auth::user()->id){
            // The user is signed in and can access the game
            return view('campaign.add-user', compact('id'));
        } else{
            return redirect()->route('BabyGotBackSlash');
        }
    }

    /**
     * Show the form for adding a user.
     *
     * @param  \Illuminate\Http\Request  $request 
     * @return \Illuminate\Http\Response
     */
    public function uniqueAdd(Request $request)
    {
        if(Auth::check() && (Campaign::find($request->campaign_id)->user_id === Auth::user()->id)) {
            if ($request->has('username')) {
                error_log("has username");
                if(User::where('name', $request->username)->first()){
                    error_log("passes rediculous risky query check");
                    $tmpC = Campaign::find($request->campaign_id);
                    $tmpC->users()->attach(User::where('name', $request->username)->first()->id);
                    $tmpC->save();
                    return view('campaign.add-user', ['resp', ['type' => 0, 'msg' => 'User successfully added to game'], 'id' => $request->campaign_id]);
                }
                return view('campaign.add-user', ['resp', ['type' => 1, 'msg' => 'Invalid user'], 'id' => $request->campaign_id]);
            }
        }
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return view('campaign.add-user', ['resp', ['type' => 1, 'msg' => 'you do not have permission to perform that action']]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
