<?php
//Ascii art from  http://lunicode.com/bigtext

namespace App;

use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
	


	
/*
______             _                ___                                       
|  ___|           (_)              / _ \                                      
| |_ ___  _ __ ___ _  __ _ _ __   / /_\ \ ___ ___ ___  ___ ___  ___  _ __ ___ 
|  _/ _ \| '__/ _ \ |/ _` | '_ \  |  _  |/ __/ __/ _ \/ __/ __|/ _ \| '__/ __|
| || (_) | | |  __/ | (_| | | | | | | | | (_| (_|  __/\__ \__ \ (_) | |  \__ \
\_| \___/|_|  \___|_|\__, |_| |_| \_| |_/\___\___\___||___/___/\___/|_|  |___/
                      __/ |                                                   
                     |___/                                                    
*/
    public function users()
    {
    	return $this->belongsToMany('App\User');
    }
    public function user() 
    {
    	return $this->belongsTo('App\User');
    }
    public function characters() 
    {
    	return $this->hasMany('App\Character');
    }
    public function rolls()
    {
    	return $this->hasMany('App\Roll');
    }
}
