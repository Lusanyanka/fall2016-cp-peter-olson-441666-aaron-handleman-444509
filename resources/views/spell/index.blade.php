@extends('layout')


{{-- Depends on an items paginator --}}
@section('content')
{{-- Check and see if the Paginator is empty --}}
@if($spells->isEmpty())
	<div class="panel panel-default game-panel">
		<div class="panel-body">
			<h2>You have no spells!</h2>
			@include('spell.make-button')
		</div>
	</div>
@else
<div class="panel panel-default game-panel">
	<div class="panel-body">
		<h2>Homebrew Spells</h2>
		@include('spell.make-button')
	</div>
</div>

<div class="row">
	<div class="col-md-offset-1">
		@foreach($spells as $spell)
			@include('spell.brief', compact('spell'))
		@endforeach
	</div>
</div>
<div class="row">
	{{$spells->links()}}
</div>
@endif
@stop