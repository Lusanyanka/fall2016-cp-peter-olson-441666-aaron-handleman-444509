{{-- Expects to have an $spell object --}}

<div class="col-md-5">
	<div class="panel panel-default game-panel">
		<div class="panel-heading">
			<h2>{{$spell->name}}</h2>
			<span> {{$spell->cost}} </span>
		</div>
		<div class="panel-body">
			<span> {{$spell->description}} </span>
			<a href="/spell/create/{{$spell->id}}" title="" class="pull-right">Use as a template</a>
		</div>
	</div>
</div>