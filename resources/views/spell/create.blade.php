@extends('layout')

@section('content')

@if(isset($id))
<?php $spell = App\Spell::find($id); ?>

<div class="row">
	<div class="col-md-6 col-md-offset-3">
		{!! Form::open(['route' => 'spell.store']) !!}
		<div class="form-group">
			<label for="name" >Spell Name: </label>
			<input type="text" id="name" name="name" class="form-control" value="{{$spell->name}}">
		</div>
		<div class="form-group">
			<label for="level">Spell Level: </label>
			{{ Form::selectRange('level', 0, 9)}}
		</div>
		<div class="form-area">
			<label for="description">Description:</label>
			<textarea name="description" id="description" name="description" class="form-control">{{$spell->description}}</textarea>
			<input type="hidden" name="creator" value="{{Auth::user()->id}}">
		</div>
		<div class="form-area pull-right buf-top">
			<button type="submit" class="btn btn-primary">Create Spell!</button>
		</div>
		{!! Form::close() !!}
	</div>
</div>

@else

<div class="row">
	<div class="col-md-6 col-md-offset-3">
		{!! Form::open(['route' => 'spell.store']) !!}
		<div class="form-group">
			<label for="name" >Spell Name: </label>
			<input type="text" id="name" name="name" class="form-control">
		</div>
		<div class="form-group">
			<label for="level">Spell Level: </label>
			{{ Form::selectRange('level', 0, 9)}}
		</div>
		<div class="form-area">
			<label for="description">Description:</label>
			<textarea name="description" id="description" name="description" class="form-control"></textarea>
			<input type="hidden" name="creator" value="{{Auth::user()->id}}">
		</div>
		<div class="form-area pull-right buf-top">
			<button type="submit" class="btn btn-primary">Create Spell!</button>
		</div>
		{!! Form::close() !!}
	</div>
</div>

@endif

@stop