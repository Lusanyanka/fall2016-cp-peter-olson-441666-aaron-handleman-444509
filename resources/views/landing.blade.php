@extends('layout')

@section('content')

@foreach ($games as $game)
	@include('bits.game-post', compact('game'))
@endforeach

@stop