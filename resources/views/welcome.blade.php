@extends('layout')

@section('header')
<style>
    ul{
        list-style: none;
    }

    li :before{
        font-family: monospace;
        content: "[O]";
    }
    li .done:before{
        content: "[X]";
        font-family: monospace;
    }
    li .nopls:before{
        font-family: monospace;
        content: "<X>  ";
    }
    .nopls{
        font-style: italic;
    }
</style>
@stop

@section('content')
    <h1>This is the DnD landing page</h1>
    <h1>Potential site hierachy:</h1>
    <p>There's no way that we'd ever be able to do this all in this project, so we should also italliac out any features that we don't want to add yet</p>
    <h1>Sign In</h1>
    <ul>
        <li></li>
        <li><h2>Games</h2></li>
        <ul>
            <li><h3>Game Description</h3></li>
            <li><h3>Characters</h3></li>
            <ul>
                <li><h4>Character sheets</h4></li>
                <ul>
                    <li>Player, name, race, alignment</li>
                    <li class="nopls">Class, level, exp</li>
                    <li class="nopls">Hitpoints, armor, and speed</li>
                    <li class="nopls">Spellcasting, spell save, and spell bonus</li>
                    <li>Big Six stats, bonuses, and skills</li>
                    <li>Cold, hard cash</li>
                    <li>Inventory</li>
                    <li class="nopls">Feats, features (just text fields)</li>
                    <li>Spells</li>
                    <li class="nopls">Attacks</li>
                </ul>
                <li><h4>Create a new character</h4></li>
                <ul>
                    <li class="nopls">Auto-rolling for $$$ and gear</li>
                    <li>Choose stats</li>
                    <li>Select gear</li>
                </ul>
            </ul>
            <li><h3 style="display: inline-block;">NPCs</h3> - Could probably just be sheets associated with the GM of the particular game</li>
            <li><h3>GM management panel</h3></li>
            <ul>
                <li><h4 class="nopls">Next session</h4></li>
                <li><h4 class="nopls">Annoucements</h4></li>
                <li><h4>Players (add/kick)</h4></li>
                <li><h4>Give inventory</h4></li>
            </ul>
        </ul>
        <li><h2>Characters</h2></li>
        <ul>
            <li><h3>Corresponding game</h3></li>
            <li><h3>Character Sheet</h3></li>
        </ul>
        <li><h2>Homebrew Content</h2></li> 
        <ul>
            <li><h3>Content Pool</h3></li>
            <li><h3>Create new content</h3></li>
            <ul>
                <li><h4>Associated game</h4></li>
                <li><h4 class="nopls">Type of gear</h4></li>
                <li><h4 class="nopls">Attributes</h4></li>
            </ul>
        </ul>
    </ul>
@stop