@extends('layouts.master_layout_aaron', ['location' => 'character'])
@section('content')
<p>
	Roll 1: {{$roll->roll_1}}
</p>
<p>
	Roll 2: {{$roll->roll_2}}
</p>
<p>
	Roll 3: {{$roll->roll_3}}
</p>
<p>
	Roll 4: {{$roll->roll_4}}
</p>
<p>
	Roll 5: {{$roll->roll_5}}
</p>
<p>
	Roll 6: {{$roll->roll_6}}
</p>
@endsection