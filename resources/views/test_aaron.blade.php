<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
        <link rel="stylesheet" href="{{elixir('css/app.css')}}">
        <script defer="defer" src="{{elixir('js/app.js')}}"> </script>
    </head>
    <body>
        <div id="app">
            <testing response="memes?">  </testing>
        </div>
        <a href={{route('home_aaron')}}> Go to Aaron Layouts </a>
        @include('inner_aaron')
    </body>
</html>