<nav class="navbar navbar-default navbar-static-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
        <span class="sr-only">Toggle Navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand">DnD Stuff</a>
    </div>
    
    <div class="collapse navbar-collapse" id="app-navbar-collapse">
      <!-- Left Side Of Navbar -->
      <ul class="nav navbar-nav">
        <ul class="nav navbar-nav">
          <li class=""><a href={{route('home')}}>Home</a></li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
              Campaigns <span class="caret"></span>
            </a>

            <ul class="dropdown-menu" role="menu">
              <li>
                <a href=""> My Campaigns <!--WILL BE FOR CAMPAIGNS, NEEDS LINKING--></a>
              </li>
              <li>
                <a href=""> Browse Campaigns <!--WILL BE FOR CAMPAIGNS, NEEDS LINKING--></a>
              </li>
              <li>
                <a href=""> Create a Campaign <!--WILL BE FOR CAMPAIGNS, NEEDS LINKING--></a>
              </li>
            </ul>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
              Characters <span class="caret"></span>
            </a>

            <ul class="dropdown-menu" role="menu">
              <li>
                <a href=""> My Characters <!--WILL BE FOR CHARACTER LINKS, NEEDS LINKING--></a>
              </li>
            </ul>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
              Homebrew <span class="caret"></span>
            </a>

            <ul class="dropdown-menu" role="menu">
              <li>
                <a href=""> Browse Spells <!--WILL BE FOR HOMEBREW LINKS, NEEDS LINKING--></a>
              </li>
              <li>
                <a href=""> Browse Items <!--WILL BE FOR HOMEBREW LINKS, NEEDS LINKING--></a>
              </li>
              <li>
                <a href=""> Create Spells <!--WILL BE FOR HOMEBREW LINKS, NEEDS LINKING--></a>
              </li>
              <li>
                <a href=""> Create Items <!--WILL BE FOR HOMEBREW LINKS, NEEDS LINKING--></a>
              </li>
            </ul>
          </li>
        </ul>
      </ul>

      <!-- Right Side Of Navbar -->
      <ul class="nav navbar-nav navbar-right">
        <!-- Authentication Links -->
        @if (Auth::guest())
        <li><a href="{{ url('/login') }}">Login</a></li>
        <li><a href="{{ url('/register') }}">Register</a></li>
        @else
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
            {{ Auth::user()->name }} <span class="caret"></span>
          </a>

          <ul class="dropdown-menu" role="menu">
            <li>
              <a href="{{ url('/logout') }}"
              onclick="event.preventDefault();
              document.getElementById('logout-form').submit();">
              Logout
            </a>

            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
              {{ csrf_field() }}
            </form>
          </li>
        </ul>
      </li>
      @endif
    </ul>
  </div>
</div>
</nav>