<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <link rel="stylesheet" href="{{elixir('css/app.css')}}">
        <script defer="defer" src="{{elixir('js/app.js')}}"> </script>
		<title> DnD CM - @yield('page_title') </title>
		@yield('header_content')
	</head>
	<body>
		@include('layouts.navbar_aaron', ['location' => $location])
		<div class="container" id="app">
            @yield('content')
        </div>
	</body>
</html>