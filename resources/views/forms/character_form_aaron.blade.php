<?php $roll_col = App\Campaign::find($campaign)->rolls()->where('user_id', Auth::user()->id)->select('roll_1', 'roll_2', 'roll_3', 'roll_4', 'roll_5', 'roll_6')->get();?>

{!! Form::open(['route' => 'character.store']) !!}
{!! Form::label('name', 'Name: ')!!}
{!! Form::text('name') !!}
{!! Form::hidden('campaign', $campaign)!!}
{!! Form::hidden('creator', Auth::user()->id) !!}

<stat-select roll_col='{{json_encode($roll_col)}}'>
</stat-select>
{!! Form::label('str_bonus', "Strength Bonus")!!}
{!! Form::number('str_bonus')!!}
{!! Form::label('dex_bonus', "Dexterity Bonus")!!}
{!! Form::number('dex_bonus')!!}
{!! Form::label('con_bonus', "Constitution Bonus")!!}
{!! Form::number('con_bonus')!!}
{!! Form::label('int_bonus', "Intelligence Bonus")!!}
{!! Form::number('int_bonus')!!}
{!! Form::label('wis_bonus', "Wisdom Bonus")!!}
{!! Form::number('wis_bonus')!!}
{!! Form::label('cha_bonus', "Charisma Bonus")!!}
{!! Form::number('cha_bonus')!!}
{!! Form::submit('Create Character!') !!}
{!! Form::close() !!}