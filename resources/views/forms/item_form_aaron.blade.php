{!! Form::open(['route' => 'campaign.store']) !!}
{!! Form::text('name') !!}
{!! Form::number('price') !!}
{!! Form::textarea('description') !!}
{!! Form::hidden('creator', Auth::user()->name) !!}
{!! Form::submit('Create Item!') !!}
{!! Form::close() !!}