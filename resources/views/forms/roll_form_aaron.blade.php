{!! Form::open(['route' => 'roll.store']) !!}
@if(isset($campaign))
{!! Form::hidden('campaign', $campaign)!!}
@else
{!! Form::text('campaign') !!}
@endif
{!! Form::hidden('username', Auth::user()->name) !!}
{!! Form::submit('Make Roll!') !!}
{!! Form::close() !!}