{!! Form::open(['route' => 'campaign.store']) !!}
{!! Form::label('name', "Name:")!!}
{!! Form::text('name') !!}
{!! Form::label('description', "Description:")!!}
{!! Form::textarea('description') !!}
{!! Form::hidden('creator', Auth::user()->name) !!}
{!! Form::submit('Create Campaign!') !!}
{!! Form::close() !!}