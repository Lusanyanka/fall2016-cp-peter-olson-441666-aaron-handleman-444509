{!! Form::open(['route' => 'character.create', 'method' => 'POST']) !!}
	<input type="hidden" name="campaign" value={{$game->id}}>
	<button type="submit" class="btn btn-primary">New Character</button>
{!! Form::close() !!}