@extends('layout')

@section('content')
<?php 
echo $campaign;
$roll_col = App\Campaign::find($campaign)->rolls()->select('roll_1', 'roll_2', 'roll_3', 'roll_4', 'roll_5', 'roll_6')->get();
?>

{{-- Make your roll --}}
<div class="row">
	<div class="col-md-6 col-md-offset-3">
		{!! Form::open(['route' => 'roll.store']) !!}
		@if(isset($campaign))
			<label for="campaign">Campaign: </label>
			<input type="text" name="__NuLl" id="__NuLl" value="{{App\Campaign::find($campaign)->name}}" readonly>
			<input type="hidden" name="campaign" value="<?php echo $campaign; ?>">
		@else
			<label for="campaign">Campaign: </label>
			{!! Form::text('campaign') !!}
		@endif
		{!! Form::hidden('username', Auth::user()->name) !!}
		{!! Form::submit('Make Roll!') !!}
		{!! Form::close() !!}
	</div>
</div>

{{-- Give the character attributes --}}
<div class="row">
	<div class="col-md-6 col-md-offset-3">
		{!! Form::open(['route' => 'character.store']) !!}
		<div class="form-group">
			<label for="name" class="col-md-4 text-right">Name:</label>
			<div class="col-md-8">
				<input type="text" name="name" id="name" class="form-control">
			</div>
		</div>
		{!! Form::hidden('campaign', $campaign)!!}
		{!! Form::hidden('creator', Auth::user()->id) !!}

		<stat-select roll_col="{{json_encode($roll_col)}}"></stat-select>
		<div class="form-group buf-top">
			<label for="str_bonus" class="col-md-4 text-right">Strength bonus:</label>
			<div class="col-md-2">
				<input type="number" name="str_bonus" id="str_bonus" class="form-control">
			</div>
			<label for="dex_bonus" class="col-md-4 text-right">Dexterity bonus:</label>
			<div class="col-md-2">
				<input type="number" name="dex_bonus" id="dex_bonus" class="form-control">
			</div>
			<label for="con_bonus" class="col-md-4 text-right">Constitution bonus:</label>
			<div class="col-md-2">
				<input type="number" name="con_bonus" id="con_bonus" class="form-control">
			</div>
			<label for="int_bonus" class="col-md-4 text-right">Intelligence bonus:</label>
			<div class="col-md-2">
				<input type="number" name="int_bonus" id="int_bonus" class="form-control">
			</div>
			<label for="wis_bonus" class="col-md-4 text-right">Wisdom bonus:</label>
			<div class="col-md-2">
				<input type="number" name="wis_bonus" id="wis_bonus" class="form-control">
			</div>
			<label for="cha_bonus" class="col-md-4 text-right">Charisma bonus:</label>
			<div class="col-md-2">
				<input type="number" name="cha_bonus" id="cha_bonus" class="form-control">
			</div>
			<button type="submit" class="btn btn-primary pull-right">Create Character</button>
			{!! Form::close() !!}
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-6 col-md-offset-3">
		{!! Form::open(['route' => 'roll.store']) !!}
		@if(isset($campaign))
		{!! Form::hidden('campaign', $campaign)!!}
		@else
		{!! Form::text('campaign') !!}
		@endif
		{!! Form::hidden('username', Auth::user()->name) !!}
		<p>You also have the option to spend your 27 extra points of credit to spread around your values everywhere, and create your own rolls.</p>
		<stat-buy> </stat-buy>
		{!! Form::submit('Select Stats!') !!}
		{!! Form::close() !!}
	</div>
</div>

@stop