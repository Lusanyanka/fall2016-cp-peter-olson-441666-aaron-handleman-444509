@extends('layout')

@section('content')

<div class="container">

@if($chars->isEmpty())

	<div class="panel panel-default game-panel">
		<div class="panel-body">
			<h2>You have no characters! Create a character from a campaign that you're part of.</h2>
		</div>
	</div>

@else

@foreach($chars as $char)
	@include('bits.character-post', compact('char'))
@endforeach

@endif

</div>

@stop