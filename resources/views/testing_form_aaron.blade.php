@extends('layouts.master_layout_aaron', ['user' => 'Aaron', 'location' => 'home'])
@section('content')
	<div class="container">
		<div class="row text-center">
			<div class="col-sm-12">
				<p>
					Campaign Create
				</p>
				@include('forms.campaign_form_aaron')
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row text-center">
			<div class="col-sm-6">
				<p>
					Roll Create
				</p>
				@include('forms.roll_form_aaron')
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row text-center">
			<div class="col-sm-6">
				<p>
					Character Create
				</p>
				@include('forms.character_form_aaron', ['campaign' => 1])
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row text-center">
			<div class="col-sm-6">
				<p>
					Point Buy
				</p>
				@include('forms.point_buy_form')
			</div>
		</div>
	</div>
@endsection