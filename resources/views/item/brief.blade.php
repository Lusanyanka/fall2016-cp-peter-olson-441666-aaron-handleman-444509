{{-- Expects to have an $item object --}}

<div class="col-md-5">
	<div class="panel panel-default game-panel">
		<div class="panel-heading">
			<h2>{{$item->name}}</h2>
			<span> {{$item->cost}} </span>
		</div>
		<div class="panel-body">
			<span> {{$item->description}} </span>
			<a href="/item/create/{{$item->id}}" title="" class="pull-right">Use as a template</a>
		</div>
	</div>
</div>