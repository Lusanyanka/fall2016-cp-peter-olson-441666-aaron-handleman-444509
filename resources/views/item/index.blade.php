@extends('layout')


{{-- Depends on an items paginator --}}
@section('content')
{{-- Check and see if the Paginator is empty --}}
@if(Auth::user()->items())
	<?php $items = Auth::user()->items()->get(); ?>
	<div class="panel panel-default game-panel">
		<div class="panel-body">
			<h2>Homebrew items</h2>
			@include('item.make-button')
		</div>
	</div>
	<div class="row">
		<div class="col-md-offset-1">
			@foreach($items as $item)
				@include('item.brief', compact('item'))
			@endforeach
		</div>
	</div>

@else
	<div class="panel panel-default game-panel">
		<div class="panel-body">
			<h2>You have no items!</h2>
			@include('item.make-button')
		</div>
	</div>

@endif

@stop