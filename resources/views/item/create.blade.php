@extends('layout')

@section('content')

@if(isset($id))
<?php $item = App\Item::find($id); ?>

<div class="row">
	<div class="col-md-6 col-md-offset-3">
		{!! Form::open(['route' => 'item.store']) !!}
		<div class="form-group">
			<label for="name" >Item Name: </label>
			<input type="text" id="name" name="name" class="form-control" value="{{$item->name}}">
		</div>
		<div class="form-group">
			<label for="cost">Item cost: </label>
			<input type="number" name="cost" id="cost" value="{{$item->cost}}">
		</div>
		<div class="form-area">
			<label for="description">Description:</label>
			<textarea name="description" id="description" name="description" class="form-control">{{$item->description}}</textarea>
			<input type="hidden" name="creator" value="{{Auth::user()->id}}">
		</div>
		<div class="form-area pull-right buf-top">
			<button type="submit" class="btn btn-primary">Create Item!</button>
		</div>
		{!! Form::close() !!}
	</div>
</div>
@else
<div class="row">
	<div class="col-md-6 col-md-offset-3">
		{!! Form::open(['route' => 'item.store']) !!}
		<div class="form-group">
			<label for="name" >Item Name: </label>
			<input type="text" id="name" name="name" class="form-control">
		</div>
		<div class="form-group">
			<label for="cost">Item cost: </label>
			<input type="number" name="cost" id="cost" value="10">
		</div>
		<div class="form-area">
			<label for="description">Description:</label>
			<textarea name="description" id="description" name="description" class="form-control"></textarea>
			<input type="hidden" name="creator" value="{{Auth::user()->id}}">
		</div>
		<div class="form-area pull-right buf-top">
			<button type="submit" class="btn btn-primary">Create Item!</button>
		</div>
		{!! Form::close() !!}
	</div>
</div>
@endif

@stop