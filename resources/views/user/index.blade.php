@extends('layout')

@section('content')

<?php $users = App\User::all(); ?>

<div class="row">
	<div class="col-md-4 col-md-offset-4 panel">
		<div class="panel-header">
			<h3>Profile Links</h3>
		</div>
		<div class="panel-body">
			<ul>
				@foreach($users as $user)
					<li><a href="/user/{{$user->id}}" title="">{{$user->name}}</a></li>
				@endforeach
			</ul>
		</div>
	</div>
</div>

@stop