@extends('layout')

@section('content')

<?php $games = $user->campaigns()->get(); ?>

<div class="row">
	<div class="col-md-4 col-md-offset-4">
		<div class="panel">
			<div class="panel-header">
				<h2>Profile:  {{$user->name}} </h2>
			</div>
			<div class="panel-body">
				<h4>Games</h4>
				<ul>
				@foreach($games as $game)
					<li><a href="/campaign/{{$game->id}}" title="">{{$game->name}}</a></li>
				@endforeach
				</ul>
			</div>
		</div>
	</div>
</div>

@stop