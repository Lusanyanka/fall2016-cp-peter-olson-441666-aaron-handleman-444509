@extends('layout')

@section('content')

@if(isset($resp))

@if($resp->type === 0)
<div class="alert alert-success">
  <strong>Success!</strong> User added.
</div>
@else

<div class="alert alert-warning">
  <strong>Warning!</strong> {{$resp->msg}}
</div>

@endif
@endif
<div class="row">
	<div class="panel col-md-6 col-md-offset-3">
		<div class="panel-header">
			<h2>Enter a user's name to add them.</h2>
		</div>
		<div class="panel-body">
			{!! Form::open(['route' => 'uniqueAdd']) !!}
				<label for="username" class="col-md-3 text-right">Username: </label>
				{!! Form::hidden('campaign_id', $id)!!}
				<div class="col-md-4">
					<input type="text" name="username" id="username">
				</div>
				<button type="submit" class="btn btn-primary">Add User</button>
			{!! Form::close() !!}
		</div>
	</div>
</div>

@stop