<div class="panel panel-default game-panel">
    <div class="panel-heading">
    	<h1>{{$game->name}}</h1>
    	<span class="game-owner"> {{$game->user->name}} </span>
    </div>

    <div class="panel-body">
        <p>{{$game->description}}</p>
    </div>
</div>
