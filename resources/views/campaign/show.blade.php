@extends('layout')

@section('content')
<div class="col">
	<div class="row">
		@include('campaign.game-post', compact('game'))

		{{-- Only print if the authenticated user is in the game --}}
		@if ($game->users->find(Auth::id()))

		<div class="panel panel-default game-panel">
			@if ($game->characters->isEmpty())
				<div class="panel-body">
					<h2>There are no characters in this game yet!</h2>
					@include('character.make-button')
				</div>
			@else
				<div class="panel-heading">
					<h2> Characters </h2>
					@if($game->characters->find(Auth::user()))
						@include('character.make-button')
					@endif
				</div>
				<div class="panel-body">
					@foreach ($game->characters as $char)
						@include('character.character-brief', compact('char'))
					@endforeach
				</div>
			@endif
		</div>
		<div class="panel panel-default game-panel">
			@if ($game->users->isEmpty())
				<div class="panel-body">
					<h2>There are no users in this game yet!</h2>
					@include('campaign.add-usr-btn', ['campaign' => $game->id])
				</div>
			@else
				<div class="panel-heading">
					<h2> Users </h2>
					@include('campaign.add-usr-btn', ['campaign' => $game->id])
				</div>
				<div class="panel-body">
					@foreach ($game->users as $user)
						@include('campaign.player', compact('user'))
					@endforeach
				</div>
				
			@endif
		</div>

		{{-- Otherwise, show only GM and description --}}
		@else
			{{-- alt behaviour --}}
		@endif
	</div>
</div>
@stop