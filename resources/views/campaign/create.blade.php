@extends('layout')

@section('content')

<div class="row">
	<div class="col-md-6 col-md-offset-3">
		{!! Form::open(['route' => 'campaign.store']) !!}
		<div class="form-group">
			<label for="name" >Name: </label>
			<input type="text" id="name" name="name" class="form-control">
		</div>
		<div class="form-area">
			<label for="description">Description:</label>
			<textarea name="description" id="description" name="description" class="form-control"></textarea>
			<input type="hidden" name="creator" value="{{Auth::user()->id}}">
		</div>
		<div class="form-area pull-right buf-top">
			<button type="submit" class="btn btn-primary">Create Campaign!</button>
		</div>
		{!! Form::close() !!}
	</div>
</div>

@stop