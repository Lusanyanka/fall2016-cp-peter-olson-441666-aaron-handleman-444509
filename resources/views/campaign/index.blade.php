@extends('layout')

@section('content')

@if($games->isEmpty())

<div class="col-md-10 col-md-offset-1">
	<div class="panel panel-default game-panel">
		<div class="panel-body">
			<h2>You have no campaigns!</h2>
			@include('campaign.make-button')
		</div>
	</div>
</div>

@else

<div class="col-md-10 col-md-offset-1">
	<div class="panel panel-default game-panel">
		<div class="panel-body">
			<h2>Wow campaigns!</h2>
			@include('campaign.make-button')
		</div>
	</div>
</div>

@foreach($games as $game)
	@include('bits.game-post')
@endforeach

@endif

@stop