<div class="col">
	<div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default game-panel">
	            <a href="campaign/{{$game->id}}" title="">
	                <div class="panel-heading">
	                	<h1>{{$game->name}}</h1>
	                	<span class="game-owner"> {{$game->user->name}} </span>
	                </div>
	            </a>

                <div class="panel-body">
                    <p>{{$game->description}}</p>
                </div>
            </div>
        </div>
    </div>
</div>