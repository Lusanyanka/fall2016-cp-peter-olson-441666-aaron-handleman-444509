<div class="col">
	<div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default game-panel">
                <a href="character/{{$char->id}}" title="">
                    <div class="panel-heading">
                        <h2>{{$char->name}}</h2>
                        <span>{{$char->class}}</span>
                    </div>
                </a>
                <div class="panel-body">
                    <span>{{$char->description}}</span>
                </div>
            </div>
        </div>
    </div>
</div>