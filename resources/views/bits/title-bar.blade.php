<header class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="/">DnD Sheet Keeper</a>
        </div>
        @if (Auth::check())
	        <ul class="nav navbar-nav">
	        	<li><a href="/campaign">Campaigns</a></li>
	        	<li><a href="/character">Characters</a></li>
	        	<li><a href="/item">Items</a></li>
	        	<li><a href="/spell">Spells</a></li>
	        	<li><a href="/user">Users</a></li>
	        </ul>
	        <ul class="nav navbar-nav navbar-right">
	        	<li><a href="{{url('/logout')}}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a>
	        	<form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form></li>
	        </ul>
	    @else
	        <ul class="nav navbar-nav navbar-right">
	        	<li><a href={{route('login')}}>Sign In</a></li>
	        	<li><a href={{route('register')}}>Register</a></li>
	        </ul>
	    @endif
    </div>
</header>